﻿using DAL.Interfaces;
using System.Data.Entity;

namespace DAL
{
    public class ProductDbContext : DbContext, IProductDbContext
    {
        public ProductDbContext(string connectionString) : base(connectionString)
        {

        }


        public virtual IDbSet<Product> Products { get; set; }

        void IProductDbContext.SaveChanges()
        {
            base.SaveChanges();
        }
    }
}
