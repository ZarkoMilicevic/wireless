﻿using DAL.Interfaces;
using System.Configuration;
using System.IO;

namespace DAL
{
    public class JSONContext : IJsonContext
    {
        public string GetJsonString()
        {
            string json;
            string path = ConfigurationManager.AppSettings["JsonFilePath"];
            using (StreamReader r = new StreamReader(path))
            {
                json = r.ReadToEnd();
            }
            return json;
        }

        public void SaveJsonString(string data)
        {
            string path = ConfigurationManager.AppSettings["JsonFilePath"];
            File.WriteAllText(path, data);
        }
    }
}
