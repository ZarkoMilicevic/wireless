﻿namespace DAL.Interfaces
{
    public interface IJsonContext
    {
        string GetJsonString();
        void SaveJsonString(string data);
    }
}
