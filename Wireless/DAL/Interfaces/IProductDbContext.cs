﻿using System;
using System.Data.Entity;

namespace DAL.Interfaces
{
    public interface IProductDbContext : IDisposable
    {
        IDbSet<Product> Products { get; set; }
        void SaveChanges();
    }
}
