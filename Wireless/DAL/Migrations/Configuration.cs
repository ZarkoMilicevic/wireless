namespace DAL.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<DAL.ProductDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DAL.ProductDbContext context)
        {
            context.Products.AddOrUpdate(DummyData.GetProducts().ToArray());
            context.SaveChanges();
        }
    }
}
