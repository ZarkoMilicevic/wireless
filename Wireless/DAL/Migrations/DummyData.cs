﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Migrations
{
    public class DummyData
    {
        public static List<DAL.Product> GetProducts()
        {
            List<DAL.Product> result = new List<DAL.Product>()
            {
                new DAL.Product()
                {
                    Name = "Product1",
                    Description = "Some Product1",
                    Category = "Category1",
                    Manufacturer = "Manufacturer1",
                    Price = 1,
                    Supplier = "Supplier1"
                },
                new DAL.Product()
                {
                    Name = "Product2",
                    Description = "Some Product2",
                    Category = "Category2",
                    Manufacturer = "Manufacturer2",
                    Price = 2,
                    Supplier = "Supplier2"
                },
            };
            return result;
        }
    }
}
