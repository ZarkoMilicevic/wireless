﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using NUnit.Framework;
using NSubstitute;
using DAL.Interfaces;
using ServiceLayer;

namespace Wireless.Tests
{
    [TestFixture]
    public class ProductServiceJSONTest
    {
        private string productsJson;

        [SetUp]
        public void Setup()
        {
            productsJson = "[{ \"Id\":0,\"Name\":\"Product1\",\"Description\":\"Some Product1\",\"Category\":\"ff\",\"Manufacturer\":\"asfa\",\"Supplier\":\"fsaf\",\"Price\":3.0}";
            productsJson += " , { \"Id\":0,\"Name\":\"Product2\",\"Description\":\"Some Product2\",\"Category\":\"ff\",\"Manufacturer\":\"asfa\",\"Supplier\":\"fsaf\",\"Price\":3.0}]";
        }

        [Test]
        public void GetProduct_ProductExists_ReturnProduct()
        {
            IJsonContext fakeDbContext = Substitute.For<IJsonContext>();
            fakeDbContext.GetJsonString().Returns(productsJson);

            ProductServiceJSON serviceDB = new ProductServiceJSON(fakeDbContext);
            Product product = serviceDB.GetProduct("Product1");

            Assert.AreEqual(product.Description, "Some Product1");
            Assert.AreEqual(product.Price, 3);

        }

        [Test]
        public void GetProduct_ProductDoesntExist_ReturnNull()
        {
            IJsonContext fakeDbContext = Substitute.For<IJsonContext>();
            fakeDbContext.GetJsonString().Returns(productsJson);

            ProductServiceJSON serviceDB = new ProductServiceJSON(fakeDbContext);
            Product product = serviceDB.GetProduct("Product3");

            Assert.IsNull(product);

        }
    }
}
