﻿using System.Collections.Generic;
using DAL;
using DAL.Interfaces;
using NUnit.Framework;
using ServiceLayer;

namespace Wireless.Tests
{
    [TestFixture]
    public class ProductServiceDbTest : BaseTestClass
    {
        private List<Product> products;

        [SetUp]
        public void Setup()
        {
            products = new List<Product>()
            {
                new Product()
                {
                    Name = "Product1",
                    Description = "Some Product1",
                    Category = "Category1",
                    Manufacturer = "Manufacturer1",
                    Price = 1,
                    Supplier = "Supplier1"
                },
                new Product()
                {
                    Name = "Product2",
                    Description = "Some Product2",
                    Category = "Category2",
                    Manufacturer = "Manufacturer2",
                    Price = 2,
                    Supplier = "Supplier2"
                },
            };
        }

        [Test]
        public void GetProduct_ProductExists_ReturnProduct()
        {
            IProductDbContext fakeDbContext = GetMockContext(products);

            ProductServiceDB serviceDB = new ProductServiceDB(fakeDbContext);
            Product product = serviceDB.GetProduct("Product1");

            Assert.AreEqual(product.Description, "Some Product1");
            Assert.AreEqual(product.Price, 1);

        }

        [Test]
        public void GetProduct_ProductDoesntExist_ReturnNull()
        {
            IProductDbContext fakeDbContext = GetMockContext(products);

            ProductServiceDB serviceDB = new ProductServiceDB(fakeDbContext);
            Product product = serviceDB.GetProduct("Product3");

            Assert.IsNull(product);

        }
    }
}
