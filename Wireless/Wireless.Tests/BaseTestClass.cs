﻿using DAL;
using DAL.Interfaces;
using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Wireless.Tests
{
    public class BaseTestClass
    {
        protected virtual IProductDbContext GetMockContext(IList<Product> products)
        {
            IDbSet<Product> dbSetProducts = GetMockDbSet<Product>(products);

            Mock<IProductDbContext> mockContext = new Mock<IProductDbContext>();
            mockContext.Setup(m => m.Products).Returns(dbSetProducts);

            return mockContext.Object;
        }

        protected virtual IDbSet<T> GetMockDbSet<T>(IList<T> data) where T : class
        {
            IQueryable<T> queryable = data.AsQueryable();
            Mock<IDbSet<T>> mockSet = new Mock<IDbSet<T>>();
            mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryable.Provider);
            mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryable.Expression);
            mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(queryable.GetEnumerator());
            mockSet.Setup(d => d.Add(It.IsAny<T>())).Callback<T>((s) => data.Add(s));

            return mockSet.Object;
        }
    }
}
