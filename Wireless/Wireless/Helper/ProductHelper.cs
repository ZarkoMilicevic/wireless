﻿using DAL;
using System.Collections.Generic;
using System.Linq;
using Wireless.Models;

namespace Wireless.Helper
{
    public class ProductHelper
    {
        //This all could be done with Automapper

        public static List<ProductVM> MapProductsToVMs(List<Product> products)
        {
            return products.Select(x => new ProductVM()
            {
                Category = x.Category,
                Description = x.Description,
                Id = x.Id,
                Manufacturer = x.Manufacturer,
                Name = x.Name,
                Price = x.Price,
                Supplier = x.Supplier
            }).ToList();
        }

        public static Product MapVmToProduct(ProductVM productVm)
        {
            return new Product()
            {
                Category = productVm.Category,
                Description = productVm.Description,
                Id = productVm.Id,
                Manufacturer = productVm.Manufacturer,
                Name = productVm.Name,
                Price = productVm.Price,
                Supplier = productVm.Supplier
            };
        }

        public static ProductVM MapProductToVm(Product product)
        {
            return new ProductVM()
            {
                Category = product.Category,
                Description = product.Description,
                Id = product.Id,
                Manufacturer = product.Manufacturer,
                Name = product.Name,
                Price = product.Price,
                Supplier = product.Supplier
            };
        }
    }
}