﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Wireless
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            if (ConfigurationManager.AppSettings["UseJsonAsDataSource"].Equals("true"))
            {
                string data = "[{ \"Id\":0,\"Name\":\"Product1\",\"Description\":\"Some Product1\",\"Category\":\"ff\",\"Manufacturer\":\"asfa\",\"Supplier\":\"fsaf\",\"Price\":3.0} " +
                    ", { \"Id\":1,\"Name\":\"Product2\",\"Description\":\"Some Product2\",\"Category\":\"ff\",\"Manufacturer\":\"asfa\",\"Supplier\":\"fsaf\",\"Price\":3.0}]";
                string path = ConfigurationManager.AppSettings["JsonFilePath"];
                File.WriteAllText(path, data);
            }
        }
    }
}
