﻿using System.Net;
using System.Web.Mvc;
using DAL;
using ServiceLayer.Interfaces;
using Wireless.Helper;
using Wireless.Models;

namespace Wireless.Controllers
{
    public class EditProductsController : Controller
    {
        private IProductService service;

        public EditProductsController(IProductService service)
        {
            this.service = service;
        }

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,Category,Manufacturer,Supplier,Price")] ProductVM productVM)
        {
            if (ModelState.IsValid)
            {
                Product product = ProductHelper.MapVmToProduct(productVM);
                this.service.SaveProduct(product);
                return RedirectToAction("Index");
            }

            return View(productVM);
        }

        //This is not ok, only string should be sent, not whole object..
        public ActionResult EditProduct(ProductVM productVM)
        {
            if (productVM.Name == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductVM product = ProductHelper.MapProductToVm(this.service.GetProduct(productVM.Name));
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,Category,Manufacturer,Supplier,Price")] ProductVM productVM)
        {
            if (ModelState.IsValid)
            {
                Product product = ProductHelper.MapVmToProduct(productVM);
                this.service.SaveProduct(product);
                return RedirectToAction("Index");
            }
            return View(productVM);
        }
    }
}
