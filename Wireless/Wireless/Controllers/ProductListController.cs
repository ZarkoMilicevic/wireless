﻿using ServiceLayer.Interfaces;
using System.Collections.Generic;
using System.Web.Mvc;
using Wireless.Helper;
using Wireless.Models;

namespace Wireless.Controllers
{
    public class ProductListController : Controller
    {
        private IProductService service;

        public ProductListController(IProductService service)
        {
            this.service = service;
        }

        // GET: ProductList
        public ActionResult Index()
        {
            List<ProductVM> products = ProductHelper.MapProductsToVMs(service.GetProducts());
            return View(products);
        }
    }
}