﻿using DAL;
using DAL.Interfaces;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace ServiceLayer
{
    public class ProductServiceJSON : IProductService
    {
        private IJsonContext db;

        public ProductServiceJSON(IJsonContext db)
        {
            this.db = db;
        }

        public Product GetProduct(string name)
        {
            string jsonString = this.db.GetJsonString();
            Product result = new Product();
            if (jsonString != null && jsonString != string.Empty)
            {
                result = JsonConvert.DeserializeObject<IEnumerable<Product>>(jsonString).FirstOrDefault(x => x.Name.StartsWith(name));
            }
            return result;
        }

        public List<Product> GetProducts()
        {
            string jsonString = this.db.GetJsonString();
            List<Product> result = new List<Product>();
            if (jsonString != null && jsonString != string.Empty)
            {
                result = JsonConvert.DeserializeObject<List<Product>>(jsonString);
            }
            return result;
        }

        public void SaveProduct(Product product)
        {
            //very slow..
            List<Product> allProducts = JsonConvert.DeserializeObject<List<Product>>(this.db.GetJsonString());
            allProducts.Add(product);
            this.db.SaveJsonString(JsonConvert.SerializeObject(allProducts));
        }
    }
}
