﻿using DAL;
using System.Collections.Generic;

namespace ServiceLayer.Interfaces
{
    public interface IProductService
    {
        List<Product> GetProducts();
        Product GetProduct(string name);
        void SaveProduct(Product product);
    }
}
