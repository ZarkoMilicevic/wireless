﻿using ServiceLayer.Interfaces;
using DAL;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace ServiceLayer
{
    public class ProductServiceDB : IProductService
    {
        private IProductDbContext db;

        public ProductServiceDB(IProductDbContext db)
        {
            this.db = db;
        }

        public List<Product> GetProducts()
        {
            return db.Products.ToList();
        }

        public Product GetProduct(string name)
        {
            return db.Products.Where(x => x.Name.StartsWith(name)).FirstOrDefault();
        }

        public void SaveProduct(Product product)
        {
            //This is a hack, not sure why EF is not doing this
            Product productToUpdate = db.Products.FirstOrDefault(x => x.Id == product.Id);
            if (productToUpdate != null)
            {
                db.Products.Remove(productToUpdate);
            }

            db.Products.Add(product);

            db.SaveChanges();
        }
    }
}
